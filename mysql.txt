CREATE TABLE product ( 
SKU varchar(15) ,
Name varchar(255) ,
    PRIMARY KEY (SKU),
price float,
   type varchar(15) 
);

CREATE TABLE book( 
SKU varchar(15) ,
PRIMARY KEY (SKU),
FOREIGN KEY (SKU) REFERENCES product(SKU) ON DELETE CASCAD,
weight float

);
CREATE TABLE CD( 
SKU varchar(15),
PRIMARY KEY (SKU),
FOREIGN KEY (SKU) REFERENCES product(SKU) ON DELETE CASCAD,
size int

);

CREATE TABLE furniture( 
SKU varchar(15) ,
PRIMARY KEY (SKU),
FOREIGN KEY (SKU) REFERENCES product(SKU) ON DELETE CASCAD,
weight float,
height float,
length float

)

