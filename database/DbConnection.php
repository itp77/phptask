<?php


    class DbConnection
    {
		protected $configs = array();
		private $pdo;
        function __construct()
        {
			$configs = include('config.php');
            try
              {
                 $this->pdo = new PDO('mysql:host='. $configs->host .';dbname='.$configs->db_name, $configs->db_username, $configs->db_password);
              }
             catch (PDOException $e)
              {
                  exit('Error Connecting To DataBase');
               }
        }
		
		function  __destruct() {
    
	        $this->pdo=NULL;
    
           }

        function query($myquery)
        {
			
            $query = $this->pdo->prepare($myquery);
            $query->execute();
            return $query->fetchAll();
        }
		
		function getDetails($table ,$SKU){
			   $sql ="SELECT * From $table where SKU='$SKU'";
				 return $this->query($sql);
		   }
    }
	
?>