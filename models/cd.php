<?php
include_once ('product.php');
class Cd extends Product
{

    private const TYPE = "CD";
    private $size;

    public function setSize($size)
    {
        $this->size = $size;
    }
    public function getSize($size)
    {
        return $this->size;
    }
	public static function getProductType()
	{
		return (Cd::TYPE);
	}
    public function save()
    {
        Product::create(Cd::TYPE);
		$sku=$this->getSku();
        $sql = "INSERT INTO cd (SKU,size) VALUES ('$sku','$this->size')";

        $this->DB->query($sql);
 
    }

}

?>
