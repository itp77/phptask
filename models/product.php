<?php
include_once __DIR__ . '/../database/DbConnection.php';
abstract class Product
{

    private $name;
    private $price;
    private $SKU;

    protected $DB;

    function __construct()
    {

        $this->DB = new DbConnection();
    }

    protected function create($type)
    {

        $sql = "INSERT INTO product (SKU, name, price,type) VALUES ('$this->SKU', '$this->name', '$this->price','$type')";

        $this->DB->query($sql);

    }

    public function setName($name)
    {
        $this->name = $name;
    }
    public function setSku($SKU)
    {
        $this->SKU = $SKU;
    }
    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getName()
    {
        return $this->name;
    }
    public function getSku()
    {
        return $this->SKU;
    }
    public function getPrice()
    {
        return $this->price;
    }

    abstract public function save();
	abstract public static function getProductType();
}

?>
