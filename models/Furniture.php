<?php
require_once ('product.php');
class Furniture extends Product
{

    private const TYPE = "Furniture";
    private $height;
    private $width;
    private $length;

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setLength($length)
    {
        $this->length = $length;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getLength()
    {
        return $this->length;
    }
    public static function getProductType()
	{
		return (Furniture::TYPE);
	} 
    public function save()
    {
        Product::create(Furniture::TYPE);
		$sku=$this->getSku();
        $sql = "INSERT INTO furniture (SKU,height,width,length) VALUES ('$sku','$this->height','$this->width','$this->length')";

        $this->DB->query($sql);
            
            
    }

}

?>
