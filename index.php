<?php
/* all the request  are routed to index.php ,from index.php request will be switched targeted controllers,
request Method POST or Get will be checked where aplicable ,

*/
include 'controllers/BookController.php';
include 'controllers/CdController.php';
include 'controllers/ListController.php';
include 'controllers/FurnitureController.php';
include 'controllers/ProductController.php';

$url = $_SERVER['REQUEST_URI'];


switch ($url)
{
    case '/s/':
        header('Location: /s/list');
    break;

    case '/s/checsku':
        {
                if ($_SERVER['REQUEST_METHOD'] == 'POST')
                {
                    $SKU = json_decode(stripslashes($_POST['data']));
                    $product_controller = new ProductController();
                    $result = $product_controller->isSkuAvailable($SKU);
                    echo $result;

                }
                else
                {
                    header('Location: /s/list');
                }
            break;
        }
    case '/s/delete':
        {

            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $SKU_list = json_decode(stripslashes($_POST['data']));
                $product_controller = new ProductController();
                $product_controller->deleteProduct($SKU_list);
			
            }
            else
            {

                header('Location: /s/list');
            }
            break;
        }

    case '/s/new':
        require __DIR__ . '/views/add_view.php';
        break;
    case '/s/addBook':
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $request = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                BookController::addBook($request);
                header('Location: /s/list');
                exit;
            }
            else
            {
                header('Location: /s/new');
            }
        }

        break;

    case '/s/addCD':
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $request = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                CdController::addCd($request);
                header('Location: /s/list');
                exit;
            }
            else
            {
                require __DIR__ . '/views/list_view.php';
            }
        }

        break;

    case '/s/addFurniture':
        {
            if ($_SERVER['REQUEST_METHOD'] == 'POST')
            {
                $request = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);

                FurnitureController::addFurniture($request);
                header('Location: /s/list');
                exit;
            }
            else
            {
                header('Location: /s/list');
            }
        }

        break;
    case '/s/list':
        {

            $list_controller = new ListController();
            $all_products=$list_controller->showList();
            require __DIR__ . '/views/list_view.php';

        }
        break;
    default:
        {
            http_response_code(404);
            header('Location: /s/list');
        }
        break;
    }
    
