<?php
include __DIR__ . '/../models/Furniture.php';
class FurnitureController
{
    public static function addFurniture($request)
    {

        $furniture = new Furniture();

        $furniture->setSku($request['SKU']);
        $furniture->setName($request['name']);
        $furniture->setPrice($request['price']);
        $furniture->setHeight($request['height']);
        $furniture->setWidth($request['width']);
        $furniture->setLength($request['length']);
        $furniture->save();

    }
}

?>
