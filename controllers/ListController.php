<?php
include_once (__DIR__ . '/../database/DbConnection.php');

class ListController
{

    protected $DB;
    function __construct()
    {
        $this->DB = new DbConnection();
    }
    function __destruct()
    {

        $this->DB = NULL;

    }

   
    public function showList()
    {

        $all_products = $this->DB->query("SELECT * FROM product ");

        return $all_products;

    }
}

?>
