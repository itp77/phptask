$(document).ready(function () {
    $("#sub").click(function () {
        $("#addform").validate({
            rules: {
                SKU: {
                    required: true,
                    rangelength: [10, 10],
                    remote: {
                        url: "/s/checsku",
                        type: "post",
                        cache: false,
                        dataType: "json",

                        data: {
                            data: function () {
                                return JSON.stringify($("#SKU").val());
                            },
                        },
                    },
                },
                name: {
                    required: true,
                },
                price: {
                    required: true,
                    number: true,
                    min: 0,
                },
            },
            messages: {
                SKU: { required: "SKU must is required ", rangelength: "SKU must be  10 characters long", remote: "SKU already exists" },
                name: " required",
                price: { required: " required", min: "Non Zero" },
            },
        });
    });
});
