/* reffer to /views/ listview.php */

function selectedSKU() {
    
    var selected = [];

    $("#container1")
        .find("input:checkbox")
        .each(function () {
            $("#container1").find("input:checked");
            if ($(this).is(":checked")) {
                selected.push($(this).attr("name"));
            }
        });

    return selected;
}

$(document).ready(function () {
    
    $(".deletelist").click(function () {
        
        var selected = [];
        selected = selectedSKU();

        if (selected.length === 0) {
            alert("Select product to delete");
            return;
        } else {
            dataString = selected;
            var jsonString = JSON.stringify(dataString);
            $.ajax({
                type: "POST",
                url: "/s/delete", // refer to index case : /s/delete
                data: { data: jsonString },
                cache: false,

                success: function (response) {
                    window.location.href = "/s/listview";
                },
            });
        }
    });
});
