<?php $title= "addproduct ";include_once 'layout/header.php'; ?>

<body>
    <form action="addBook" method="post" id="addform">
        <label for="SKU">SKU: </label>
        <input type="text" id="SKU" name="SKU" /><br />
        <br />
        <label for="name">Name: </label>
        <input type="text" id="name" name="name" /><br />
        <br />
        <label for="price">Price: </label>
        <input type="number" id="price" name="price" oninput="validity.valid||(value='');" step="0.01" min="0" /><br />
        <br />

        <label for="product">Type switcher:</label>
        <select name="product" id="product">
            <option value="Book">Book</option>
            <option value="CD">CD</option>
            <option value="Furniture">Furniture</option>
        </select>
        <br />
        <br />

        <div id="x">
            <div id="book_atr" class="myDIV">
                <label for="Weight">weight: </label>
                <input type="number" id="weight" name="weight" oninput="validity.valid||(value='');" step="0.01" min="0" required /><br />
                <br />
            </div>
            <div class="hide">Please provide weigt in killograms</div>
        </div>

        <input type="submit" value="Submit" id="sub" />
    </form>
</body>

<?php include_once 'layout/footer.php';?>